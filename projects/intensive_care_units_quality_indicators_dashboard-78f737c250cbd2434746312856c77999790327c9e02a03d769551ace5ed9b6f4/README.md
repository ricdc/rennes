## Overview

<a href="https://linkr.interhop.org/blog/images/icu_qi_dashboard.png" target="_blank">
    <img src="https://linkr.interhop.org/blog/images/icu_qi_dashboard.png" style="width: 100%; height: auto; display: block; margin: 20px auto;" alt="Dashboard" />
</a>
This dashboard provides a **visualization** of the **activity** in **intensive care units**.

One of its advantages is that it can be **configured** directly **by clinicians** without the need for prior programming knowledge.

It is organized into several tabs:

- **Demographics**: This section shows the number of patients admitted during the selected period, as well as their demographic information (age, gender), mortality rate, number of admissions, readmission rate, and the main ICD-10 diagnoses of the stays.

- **Ventilation**: This section provides data on mechanical ventilation, including the number of patients on ventilators, the duration of ventilation, the extubation failure rate, the self-extubation rate, and the selected ventilation parameters.

- **Sedation**: Here you will find the medications used for sedation, the total duration of sedation, neuroleptic consumption, etc.

- **Dialysis**: This section provides information on the number of patients undergoing dialysis and the type of dialysis used.

<br />

## Installation

To install this project, you can follow the "<a href="https://linkr.interhop.org/docs/overview/">Setup</a>" tutorial in the documentation.

You can also follow these tutorials if you wish to import your own data:

- <a href="https://linkr.interhop.org/docs/installation/" target="_blank">Install LinkR</a>
- <a href="https://linkr.interhop.org/docs/import_data/" target="_blank">Import data</a>
- <a href="https://linkr.interhop.org/docs/explore/install_component/" target="_blank">Install a project</a>. Select the DOMASIA laboratory in the city of Rennes (France), and you will see the project "Intensive care units quality indicators dashboard" appear under the projects.

## Next Steps

For now, only the "Demographics" section is available, with other sections currently under development.

This dashboard is configured to visualize intensive care data. The next step will be to apply this dashboard model to **other hospital departments** and **private medical practices**.
