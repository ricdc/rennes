# DOMASIA

DOMASIA stands for **Big Data and Learning Health Systems** (DOnnées MAssives et Système d’Information Apprenant en santé).

<a href="https://ltsi.univ-rennes.fr/domasia" target="_blank">DOMASIA</a> is a team within the <a href="https://ltsi.univ-rennes.fr/domasia" target="_blank">LTSI laboratory</a>,
led by Professor Marc Cuggia.

The team specializes in **Learning Health Systems** (LHS). LHS embody the **integration** of 
**clinical practice**, **research**, and **knowledge application**.

Recognized as a major advancement in health informatics and technology,
LHS have been attracting significant attention since their introduction in 2007.
This interest has been fueled by the widespread adoption of Electronic Health Records (EHR) and
improvements in machine learning algorithms.

Central to LHS is the **extraction of new knowledge from data**, which is then fed back into
healthcare practices to **enhance care delivery**.

This git repository is the shared content produced with the <a href="https://linkr.interhop.org/" target="_blank">LinkR data science platform</a>.

<span id='generated_code_start'></span>

## <i class='fa fa-file-alt' style='color: steelblue; margin-right: 5px;'></i> Projects

<details style = 'border: solid 1px #c0c0c0; padding: 5px 10px; margin: 5px 0;'>
<summary><span style = 'font-size:13px;'>ICU quality indicators dashboard</summary>

# Installation

To install this project, you'll have to follow these steps :

- <a href="https://linkr.interhop.org/en/docs/installation/" target="_blank">Install LinkR</a>
- <a href="https://linkr.interhop.org/en/docs/import_data/" target="_blank">Import data</a>
- <a href="https://linkr.interhop.org/en/docs/explore/install_component/" target="_blank">Install this project</a>

# Overview

This dashboard presents **quality indicator** data for **intensive care units**, organized into several tabs:

- **Demographics**: This section shows the number of patients admitted over the chosen period,
along with their  demographic information (age, sex), mortality rate, number of admissions,
readmission rate, and primary ICD-10 diagnoses for the stays.

- **Ventilation**: This section provides data on mechanical ventilation, including the number of
patients on ventilators, duration of ventilation, extubation failure rate, auto-extubation rate,
and selected ventilatory parameters.

</details>

## <i class='fa fa-terminal' style='color: steelblue; margin-right: 5px;'></i> Plugins

No plugins available.

## <i class='fa fa-database' style='color: steelblue; margin-right: 5px;'></i> Datasets

No datasets available.

## <i class='fa fa-code' style='color: steelblue; margin-right: 5px;'></i> Data cleaning scripts

No data cleaning scripts available.

<span id='generated_code_end'></span>
